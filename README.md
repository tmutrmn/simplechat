# Simple real time chat app using websockets

Made with Ionic 4 + Node.js and Typescript / ES6
As an example how to use: sockets with rxjs, custom components, custom routing in Ionic 4

npm i
...
run server node server.js
run ionic serve or build...
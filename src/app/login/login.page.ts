import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from "@angular/router";

import { ChatService } from '../services/chat.service';


@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
	private nickname:string = '';

	constructor(private router: Router, private chatService: ChatService) {

	}


	ngOnInit() {

	}


	async login(nickname) {
		if (!nickname) return
		this.chatService.setNickName(nickname)
		this.router.navigate(['tabs/tab2'])
	}

}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
	{ path: '', loadChildren: './login/login.module#LoginPageModule' },				// note the order!
	{ path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
	{ path: 'login', loadChildren: './login/login.module#LoginPageModule' },
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
	],
	exports: [
		RouterModule
	]
})

export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ChatComponent } from './chat/chat.component'


@NgModule({
	imports: [
		IonicModule,
		FormsModule,
		CommonModule,
	],
	declarations: [
		ChatComponent,
	],
	exports: [
		ChatComponent,
	]
})

export class ComponentsModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';

import { ChatService } from '../../services/chat.service';


@Component({
	selector: 'app-chat',
	templateUrl: './chat.component.html',
	styleUrls: ['./chat.component.scss'],
	// providers: [ChatService],
})

export class ChatComponent implements OnInit, OnDestroy {
	private connection
	public messages = []
	public message
	private nickname = ''


	constructor(private chatService: ChatService) {
		this.nickname = chatService.getNickName()
	}


	sendMessage() {
		this.chatService.sendMessage(this.message)
		this.message = ''
	}


	ngOnInit() {
		this.connection = this.chatService.getMessages().subscribe(message => {
			this.messages.push(message)
		})
	}


	ngOnDestroy() {
		this.connection.unsubscribe()
	}

}

import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Socket } from 'ngx-socket-io';


@Injectable({
	providedIn: 'root'
})

export class ChatService {
	private nickname	// : string = '';

	constructor(private socket: Socket) {
		// console.log("constructor: " + this.nickname)
	}


	setNickName(nick) {
		this.nickname = nick
	}

	getNickName() {
		return this.nickname
	}


	sendMessage(msg) {
		if (!msg) return
		const now = new Date()
		// this.socket.emit('ADD_MSG', "[" + now.toLocaleString('fi-FI') + "] " + this.nickname + ": " + msg)		// should send an object instead!
		this.socket.emit('ADD_MSG', { timestamp: now.toLocaleString('fi-FI'), nickname: this.nickname, message: msg });
	}


	getMessages() {
		const observable = new Observable(observer => {
			this.socket.on('MSG', (data) => {
				observer.next(data)
			})
			return () => {
				this.socket.disconnect()
			}
		})
		return observable
	}

}

let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);


io.on('connection', (socket) => {
  console.log('user connected');

  socket.on('disconnect', () => {
    console.log('user disconnected');
  });

  socket.on('ADD_MSG', (message) => {
		// io.emit('MSG', { type:'new-message', text: message });
		io.emit('MSG', message);
  });
});


http.listen(3000, () => {
  console.log('started on port 3000');
});
